# Heart-Line

This main folder contains the web application for the Heart-Line project.

## Relevant folders:
- `docs/`	Relevant documents for the implementation.
- `heartline_wear/protocolo.cpp`	It's an implementation example of the communication protocol.

## Notes
- This project was created with Hanumat Framework (https://hanumt.carpathialab.com). The Framework files was not included in this packages, but are free software and can be downloaded from the Framework website.

- An example of the database structure was added to the project.

## LICENSE
- This project is FREE SOFTWARE under the GPL 3.0 https://www.gnu.org/licenses/gpl-3.0.html
