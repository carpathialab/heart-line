-- MySQL dump 10.17  Distrib 10.3.22-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: carpathialab.com    Database: heartline
-- ------------------------------------------------------
-- Server version	10.3.22-MariaDB-0+deb10u1-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `equipo`
--

DROP TABLE IF EXISTS `equipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `equipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `actor` enum('Explorer','Relative') DEFAULT NULL,
  `nombre` varchar(150) NOT NULL COMMENT 'Owner name:',
  `idmision` int(11) NOT NULL COMMENT 'Mission:|C mision,id,nombre',
  `activo` char(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipo`
--

LOCK TABLES `equipo` WRITE;
/*!40000 ALTER TABLE `equipo` DISABLE KEYS */;
INSERT INTO `equipo` VALUES (1,NULL,'José Hernández',1,'1');
/*!40000 ALTER TABLE `equipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `explorador_familiar`
--

DROP TABLE IF EXISTS `explorador_familiar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `explorador_familiar` (
  `idexplorador` int(11) NOT NULL,
  `idfamiliar` int(11) NOT NULL,
  PRIMARY KEY (`idexplorador`,`idfamiliar`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `explorador_familiar`
--

LOCK TABLES `explorador_familiar` WRITE;
/*!40000 ALTER TABLE `explorador_familiar` DISABLE KEYS */;
/*!40000 ALTER TABLE `explorador_familiar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mision`
--

DROP TABLE IF EXISTS `mision`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mision` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL COMMENT 'Mission name:',
  `finicio` date NOT NULL COMMENT 'Mission started:',
  `ffinal` date NOT NULL COMMENT 'Mission finish date:',
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mision`
--

LOCK TABLES `mision` WRITE;
/*!40000 ALTER TABLE `mision` DISABLE KEYS */;
INSERT INTO `mision` VALUES (1,'Mars Human Exploration','2021-04-01','2023-12-22');
/*!40000 ALTER TABLE `mision` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pagina_rol`
--

DROP TABLE IF EXISTS `pagina_rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pagina_rol` (
  `idrol` int(11) NOT NULL,
  `pagina` varchar(200) NOT NULL COMMENT 'Página:|T select',
  `leer` char(1) NOT NULL DEFAULT '0' COMMENT 'Leer',
  `cambiar` char(1) NOT NULL DEFAULT '0' COMMENT 'Cambiar',
  `borrar` char(1) NOT NULL DEFAULT '0' COMMENT 'Borrar',
  PRIMARY KEY (`idrol`,`pagina`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pagina_rol`
--

LOCK TABLES `pagina_rol` WRITE;
/*!40000 ALTER TABLE `pagina_rol` DISABLE KEYS */;
INSERT INTO `pagina_rol` VALUES (1,'inicio.php','1','1','1'),(1,'clientes.php','1','1','1'),(1,'usuarios.php','1','1','1'),(1,'biometrics.php','1','1','0'),(1,'devices.php','1','1','0'),(-1,'inicio.php','1','1','0');
/*!40000 ALTER TABLE `pagina_rol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rol`
--

DROP TABLE IF EXISTS `rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rol` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) DEFAULT NULL COMMENT 'Nombre del rol',
  `pagina_inicial` varchar(200) NOT NULL DEFAULT '/inicia.php' COMMENT 'Página donde inicia',
  `activo` char(1) DEFAULT '1' COMMENT 'Activo',
  PRIMARY KEY (`id`),
  UNIQUE KEY `nombre_UNIQUE` (`nombre`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rol`
--

LOCK TABLES `rol` WRITE;
/*!40000 ALTER TABLE `rol` DISABLE KEYS */;
INSERT INTO `rol` VALUES (1,'Administrator','inicio.php','1'),(2,'Explorer','inicio.php','1'),(3,'Family member','inicio.php','1');
/*!40000 ALTER TABLE `rol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_biometrico`
--

DROP TABLE IF EXISTS `tipo_biometrico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_biometrico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` char(20) NOT NULL COMMENT 'Biometric name:',
  `identificador` enum('Male','Female','All') DEFAULT NULL COMMENT 'Identifier',
  `hora` enum('Morning','Night','All','Auto') DEFAULT 'Auto' COMMENT 'Moment to ask:',
  `tipo` enum('Electrodermic','Poll','NULL') DEFAULT NULL COMMENT 'Type',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_biometrico`
--

LOCK TABLES `tipo_biometrico` WRITE;
/*!40000 ALTER TABLE `tipo_biometrico` DISABLE KEYS */;
INSERT INTO `tipo_biometrico` VALUES (1,'Sleep hours','All','Morning','Poll'),(2,'Energy','All','All','Poll'),(3,'Emotions','All','All','Poll'),(4,'Digestion','All','Night','Poll'),(5,'WC','All','Night','Poll'),(6,'Cravings','All','Night','Poll'),(7,'Sex','All','Night','Poll'),(8,'Hair','All','Night','Poll'),(9,'Skin','All','Night','Poll'),(10,'Vagina discharge','Female','Night','Poll'),(11,'Mestruation','Female','Night','Poll'),(12,'Pain','All','All','Poll'),(13,'Social','All','Night','Poll'),(14,'Mental','All','Night','Poll'),(15,'Exercise','All','Night','Poll'),(16,'Activities/Events','All','Night','Poll'),(17,'Parties','All','Night','Poll'),(18,'Diseases','All','All','Poll'),(19,'Medicines','All','All','Poll'),(20,'Heart rate','','','Electrodermic'),(21,'Body temperature','','','Electrodermic'),(22,'Stress','','','Electrodermic'),(23,'Alarm button','','','Electrodermic');
/*!40000 ALTER TABLE `tipo_biometrico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(150) NOT NULL COMMENT 'Email:',
  `passwd` varchar(41) NOT NULL,
  `nombrecompleto` varchar(200) NOT NULL COMMENT 'User name:',
  `idrol` int(11) DEFAULT NULL COMMENT 'User rol:|C rol,id,nombre',
  `momento_alta` timestamp NULL DEFAULT current_timestamp(),
  `activo` char(1) NOT NULL DEFAULT '1' COMMENT 'Active:',
  `tel_celular` char(10) DEFAULT NULL COMMENT 'Cel:',
  `sexo` enum('Femenino','Masculino') DEFAULT NULL COMMENT 'Sex:',
  `archivo` mediumblob NOT NULL COMMENT 'Picture',
  `archivo_mime` varchar(45) NOT NULL DEFAULT 'image/jpeg',
  `imeiequipo` char(15) DEFAULT NULL COMMENT 'Equipo:|C equipo,imei,nombre',
  `idmision` int(11) DEFAULT NULL COMMENT 'Mission asigned:|C mision,id,nombre',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'admin@heartline.com','*0B545035EC1FD4D9C76D02C66D208F551A756A5B','Admin de pruebas',1,NULL,'1',NULL,NULL,'','image/jpeg',NULL,NULL),(2,'nathaliecortz@gmail.com','*0B545035EC1FD4D9C76D02C66D208F551A756A5B','Nathalie Cortez',2,'2020-05-31 01:32:01','1','',NULL,'','image/jpeg',NULL,1),(3,'cindy@heartline.com','*0B545035EC1FD4D9C76D02C66D208F551A756A5B','Cindy Rodriguez',3,'2020-05-31 18:54:52','1','','Femenino','','image/jpeg',NULL,NULL);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario_biometrico`
--

DROP TABLE IF EXISTS `usuario_biometrico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario_biometrico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imeiequipo` char(15) NOT NULL,
  `valor` char(15) DEFAULT NULL,
  `idtipo_biometrico` int(11) NOT NULL,
  `mood` char(15) DEFAULT NULL,
  `momento` timestamp NOT NULL DEFAULT current_timestamp(),
  `momentoequipo` datetime DEFAULT NULL,
  `voltaje` decimal(4,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `imeiequipo` (`imeiequipo`,`momentoequipo`),
  KEY `kIMEI` (`imeiequipo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario_biometrico`
--

LOCK TABLES `usuario_biometrico` WRITE;
/*!40000 ALTER TABLE `usuario_biometrico` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuario_biometrico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `vc_equipo`
--

DROP TABLE IF EXISTS `vc_equipo`;
/*!50001 DROP VIEW IF EXISTS `vc_equipo`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vc_equipo` (
  `id` tinyint NOT NULL,
  `Device Role` tinyint NOT NULL,
  `Name` tinyint NOT NULL,
  `Mission:` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vc_mision`
--

DROP TABLE IF EXISTS `vc_mision`;
/*!50001 DROP VIEW IF EXISTS `vc_mision`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vc_mision` (
  `id` tinyint NOT NULL,
  `Mission name` tinyint NOT NULL,
  `Starts` tinyint NOT NULL,
  `Finishes` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vc_rol`
--

DROP TABLE IF EXISTS `vc_rol`;
/*!50001 DROP VIEW IF EXISTS `vc_rol`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vc_rol` (
  `id` tinyint NOT NULL,
  `nombre` tinyint NOT NULL,
  `pagina_inicial` tinyint NOT NULL,
  `activo` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vc_tipo_biometrico`
--

DROP TABLE IF EXISTS `vc_tipo_biometrico`;
/*!50001 DROP VIEW IF EXISTS `vc_tipo_biometrico`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vc_tipo_biometrico` (
  `id` tinyint NOT NULL,
  `Biometric name` tinyint NOT NULL,
  `Gender aplied` tinyint NOT NULL,
  `Moment to ask` tinyint NOT NULL,
  `Type` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `vc_usuario`
--

DROP TABLE IF EXISTS `vc_usuario`;
/*!50001 DROP VIEW IF EXISTS `vc_usuario`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `vc_usuario` (
  `id` tinyint NOT NULL,
  `Full Name:` tinyint NOT NULL,
  `e-mail` tinyint NOT NULL,
  `Security role` tinyint NOT NULL,
  `Cell phone` tinyint NOT NULL,
  `Sex` tinyint NOT NULL,
  `Mission` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `virt_usuario`
--

DROP TABLE IF EXISTS `virt_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `virt_usuario` (
  `idusuario` int(11) NOT NULL,
  `nombrecompleto` varchar(200) NOT NULL,
  `rol` varchar(20) DEFAULT NULL,
  `permisos` varchar(2000) DEFAULT NULL,
  `email` varchar(140) NOT NULL,
  `passwd` varchar(41) NOT NULL,
  `privada` varchar(700) DEFAULT NULL,
  `ultimo_login` timestamp NULL DEFAULT NULL,
  `pagina_inicial` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idusuario`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `virt_usuario`
--

LOCK TABLES `virt_usuario` WRITE;
/*!40000 ALTER TABLE `virt_usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `virt_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `vc_equipo`
--

/*!50001 DROP TABLE IF EXISTS `vc_equipo`*/;
/*!50001 DROP VIEW IF EXISTS `vc_equipo`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`fareden`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `vc_equipo` AS select `eq`.`id` AS `id`,`eq`.`actor` AS `Device Role`,`eq`.`nombre` AS `Name`,`mi`.`nombre` AS `Mission:` from (`equipo` `eq` left join `mision` `mi` on(`eq`.`idmision` = `mi`.`id`)) where `eq`.`activo` = '1' */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vc_mision`
--

/*!50001 DROP TABLE IF EXISTS `vc_mision`*/;
/*!50001 DROP VIEW IF EXISTS `vc_mision`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`fareden`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `vc_mision` AS select `mision`.`id` AS `id`,`mision`.`nombre` AS `Mission name`,`mision`.`finicio` AS `Starts`,`mision`.`ffinal` AS `Finishes` from `mision` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vc_rol`
--

/*!50001 DROP TABLE IF EXISTS `vc_rol`*/;
/*!50001 DROP VIEW IF EXISTS `vc_rol`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`fareden`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `vc_rol` AS select `rol`.`id` AS `id`,`rol`.`nombre` AS `nombre`,`rol`.`pagina_inicial` AS `pagina_inicial`,`rol`.`activo` AS `activo` from `rol` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vc_tipo_biometrico`
--

/*!50001 DROP TABLE IF EXISTS `vc_tipo_biometrico`*/;
/*!50001 DROP VIEW IF EXISTS `vc_tipo_biometrico`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`fareden`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `vc_tipo_biometrico` AS select `tipo_biometrico`.`id` AS `id`,`tipo_biometrico`.`nombre` AS `Biometric name`,`tipo_biometrico`.`identificador` AS `Gender aplied`,`tipo_biometrico`.`hora` AS `Moment to ask`,`tipo_biometrico`.`tipo` AS `Type` from `tipo_biometrico` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vc_usuario`
--

/*!50001 DROP TABLE IF EXISTS `vc_usuario`*/;
/*!50001 DROP VIEW IF EXISTS `vc_usuario`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`fareden`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `vc_usuario` AS select `usr`.`id` AS `id`,`usr`.`nombrecompleto` AS `Full Name:`,`usr`.`email` AS `e-mail`,`rol`.`nombre` AS `Security role`,`usr`.`tel_celular` AS `Cell phone`,`usr`.`sexo` AS `Sex`,`mi`.`nombre` AS `Mission` from ((`usuario` `usr` join `rol` on(`usr`.`idrol` = `rol`.`id`)) left join `mision` `mi` on(`usr`.`idmision` = `mi`.`id`)) where `usr`.`activo` = 1 */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-31 23:00:27
