<?php
function dameMenuApp() {
	$menu = '<ul class="nav navbar-nav navbar-right">
		<li class="nav-item"><a class="nav-link" href="inicio.php"> <i class="material-icons">home</i> </a></li>
		<li class="nav-item"><a class="nav-link" href="missions.php">Missions</a></li>
		<li class="nav-item"><a class="nav-link" href="devices.php">Devices</a></li>
		<li class="nav-item"><a class="nav-link" href="biometrics.php">Biometrics</a></li>
		<li class="nav-item"><a class="nav-link" href="usuarios.php">Users</a></li>
		<li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" id="menuSistema" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Maintenance</a>
			<div class="dropdown-menu" aria-labelledby="menuSistema">
				<a class="dropdown-item" href="roles.php">Security roles</a>
			</div>
		</li>
		<li class="nav-item"><a class="nav-link" href="#" onclick="doLogout()"> Exit </a></li>
	</ul>';
	return $menu;
}
?>
