#ifndef PROTOCOLO_H
#define PROTOCOLO_H


class Protocolo {
public:
    Protocolo();
    struct Paquete {
        Paquete();
        bool isBinary;
        unsigned usrID;
        bool rep;
        double lat;
        double lng;
        unsigned course;
        unsigned speed;
        unsigned hr;
        unsigned bodyTemp;
        unsigned stress;
        unsigned delay;
    };
    Paquete * read(char* paq);
};

#endif // PROTOCOLO_H
