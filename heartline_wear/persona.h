#ifndef PERSONA_H
#define PERSONA_H

#include <QFrame>

namespace Ui {
class Persona;
}

class Persona : public QFrame
{
    Q_OBJECT

public:
    explicit Persona(QWidget *parent = nullptr);
    ~Persona();

private:
    Ui::Persona *ui;
};

#endif // PERSONA_H
