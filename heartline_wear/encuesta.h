#ifndef ENCUESTA_H
#define ENCUESTA_H

#include <QDialog>

namespace Ui {
class Encuesta;
}

class Encuesta : public QDialog
{
    Q_OBJECT

public:
    explicit Encuesta(QWidget *parent = nullptr);
    ~Encuesta();

private:
    Ui::Encuesta *ui;
};

#endif // ENCUESTA_H
