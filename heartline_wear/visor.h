#ifndef VISOR_H
#define VISOR_H

#include <QDialog>

namespace Ui {
class Visor;
}

class Visor : public QDialog
{
    Q_OBJECT

public:
    explicit Visor(QWidget *parent = nullptr);
    ~Visor();

private:
    Ui::Visor *ui;
};

#endif // VISOR_H
