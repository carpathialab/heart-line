#include "visor.h"
#include "ui_visor.h"

Visor::Visor(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Visor)
{
    ui->setupUi(this);
}

Visor::~Visor()
{
    delete ui;
}
