#include "protocolo.h"

Protocolo::Protocolo() {
}

Protocolo::Paquete * Protocolo::read(char* paq) {
   /* Rules:
    * All package are 16 byte long.
    * All packages starts first three bits on.
    */

    Protocolo::Paquete * retval = new Protocolo::Paquete();
    if ((paq[0] & 0b11100000) == 0b11100000) {
        //OK, it is a valid packet.
        retval->isBinary = ((paq[0] & 0b00010000) == 0b00010000);
        retval->usrID = (paq[0] & 0b00001111) << 1 | paq[1] >> 7;
        retval->rep = (paq[1] & 0b01000000) == 0b01000000;
        if (!retval->isBinary) {
            int latInt = (paq[1] & 0b00111111) << 2 | paq[2] >> 6;
            int latPre = (paq[2] & 0b00111111) << 16 | paq[3] << 8 | (paq[4] >> 5);
            retval->lat = latInt + latPre / 10000;

        }
    }
    return retval;
}
